<?php

namespace Drupal\google_stt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Site\Settings;

/**
 * Class SpeechToTextSettingsForm.
 */
class SpeechToTextSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Site\Settings definition.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructs a new SpeechToTextSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Settings $settings
  ) {
    parent::__construct($config_factory);
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_stt.speechtotextsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'speech_to_text_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_stt.speechtotextsettings');
    $form['bucket_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Storage Bucket Name'),
      '#description' => $this->t('Your google storage bucket name. If you don\'t have one create a google storage bucket by signing into your Google Developer Console. Example : gs://your-google-bucket (Add the value &quot;your-google-bucket&quot; here.)'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('bucket_name'),
    ];
    // Check if Google Service Account credentials json file path is set.
    $credentials_path = $this->settings->get('credentials_path');
    if (empty($credentials_path)) {
      $this->messenger()
        ->addWarning($this->t('The Google API Credentials file is missing. ' .
          'Go <a href="https://console.cloud.google.com/apis/credentials" target="_blank">here</a>' .
          ' and do that. More information <a href="https://cloud.google.com/speech-to-text/docs/quickstart" target="_blank">here</a> on how to do that. ' .
          'Set path to Google API service Credentials file in settings.php. Example: $settings[\'credentials_path\'] = \'sites/default/service-account.json\';'));
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('google_stt.speechtotextsettings')
      ->set('bucket_name', $form_state->getValue('bucket_name'))
      ->save();
  }

}
