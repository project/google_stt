<?php

namespace Drupal\google_stt\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;
use Google\Cloud\Speech\V1\SpeechClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Google\Cloud\Storage\StorageClient;
use Drupal\Core\Site\Settings;

/**
 * Class SpeechToTextForm.
 */
class SpeechToTextForm extends FormBase {

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Google storage bucket url.
   *
   * @var string
   */
  private $uri;

  /**
   * Entity type manager service to load file.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The settings service to get google service account credentials from
   * settings.php.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructs a new SpeechToTextForm object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Site\Settings $settings
   */
  public function __construct(
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    Settings $settings
  ) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('entity_type.manager'),
      $container->get('settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'speech_to_text_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check if Google Service Account credentials json file is present.
    $credentials_path = $this->settings->get('credentials_path');
    if (empty($credentials_path)) {
      $this->messenger()
        ->addWarning($this->t('The Google API Credentials file is missing. ' .
          'Go <a href="https://console.cloud.google.com/apis/credentials" target="_blank">here</a>' .
          ' and do that. More information <a href="https://cloud.google.com/speech-to-text/docs/quickstart" target="_blank">here</a> on how to do that.'));
    }
    $form['language_code'] = [
      '#type' => 'select',
      '#title' => 'Language Code',
      '#options' => [
        'en-US' => 'en-US',
        'en-UK' => 'en-UK',
        'en-IN' => 'en-IN',
      ],
      '#description' => 'Choose the English language variant.',
      '#default_value' => 'en-US',
      '#required' => TRUE,
    ];

    $form['upload_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload File'),
      '#description' => $this->t('Upload the Audio/Video file to be transcribed. Currently supported file types: .wav, .flac, .m4a, .mp4, .mp3'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['mp3 mp4 m4a flac wav mpeg ogg m4p m4v avi wmv mov'],
        'file_validate_size' => [1024 * 1024 * 1024],
      ],
      '#upload_location' => 'public://audio_files',
    ];

    $form['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Transcribe'),
      '#ajax' => [
        'callback' => '::transcribeFile',
        'wrapper' => 'transcribe-output',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Transcribing...'),
        ],
      ],
    ];
    $form['#suffix'] = '<div id="transcribe-output"></div>';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $fid = reset($form_state->getValue('upload_file'));
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $fname = $file->getFilename();
    if ($fname == trim($fname) && strpos($fname, ' ') !== FALSE && $triggering_element['#name'] != 'upload_file_remove_button') {
      $form_state->setError($form['upload_file'], 'The filename has spaces, until I write the code to include spaces in filenames please rename your file.');
    }
  }

  public function transcribeFile(array &$form, FormStateInterface $form_state) {
    $credentials_path = $this->settings->get('credentials_path');
    putenv("GOOGLE_APPLICATION_CREDENTIALS=$credentials_path");
    // Display result.
    $fid = reset($form_state->getValue('upload_file'));
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $real_path = $this->fileSystem->realpath($file->getFileUri());
    $real_name = $this->fileSystem->basename($file->getFileUri());
    $name = explode('.', $real_name)[0];
    $config = $this->config('google_stt.speechtotextsettings');
    $bucket = $config->get('bucket_name');
    $this->uri = 'gs://' . $bucket . '/' . $name . '.wav';
    $output_file = $this->convertVideo($real_path, $real_name);
    $this->uploadFile($output_file, $bucket);

    list($config, $audio) = $this->setAudioDeets($output_file, $form_state->getValue('language_code'));
    // @todo Recursively append to div, every time a transcript is received.
    $transcript = $this->recognizeOperation($config, $audio);

    shell_exec("rm $output_file");
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#transcribe-output', "<h3>Transcript: </h3> $transcript"));
    return $response;
  }

  public function uploadFile($output_file, $bucket) {
    $storage = new StorageClient();
    $bucket = $storage->bucket($bucket);
    // Upload a file to the bucket.
    $bucket->upload(
      fopen($output_file, 'r')
    );
  }

  public function convertVideo($real_path, $file_name) {
    $name = explode('.', $file_name)[0];
    $output_file = $this->fileSystem->realpath(file_default_scheme() . "://");
    $output_file .= "/audio_files/$name.wav";
    shell_exec("ffmpeg -y -i $real_path -ac 1 $output_file");
    return $output_file;
  }

  public function setAudioDeets($output_file, $languageCode) {
    $audioDeets = json_decode(shell_exec("ffprobe -v quiet -print_format json -show_format -show_streams $output_file"), TRUE);
    $audio = (new RecognitionAudio())
      ->setUri($this->uri);
    // set config
    $config = (new RecognitionConfig())
      ->setEncoding(AudioEncoding::LINEAR16)
      ->setLanguageCode((string) $languageCode)
      ->setAudioChannelCount(1);

    return [$config, $audio];
  }

  public function recognizeOperation($config, $audio) {
    $client = new SpeechClient();
    // create the asynchronous recognize operation
    $operation = $client->longRunningRecognize($config, $audio);
    $operation->pollUntilComplete();
    $output_transcript = '';
    if ($operation->operationSucceeded()) {
      $response = $operation->getResult();

      // each result is for a consecutive portion of the audio. iterate
      // through them to get the transcripts for the entire audio file.
      foreach ($response->getResults() as $result) {
        $alternatives = $result->getAlternatives();
        $mostLikely = $alternatives[0];
        $transcript = $mostLikely->getTranscript();
        $output_transcript .= $transcript;
      }
    }
    else {
      $output_transcript = $operation->getError()->getMessage();
    }
    $client->close();
    return $output_transcript;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

}
