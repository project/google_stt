Google Speech To Text 8.x-1.x
--------------------------

### About this Module

A module that provides a way to consume the Google Speech to Text API. Using the form at /transcribe you can upload audio/video files and it will transcribe and print the output.


### Goals

- Make it compatible with all audio/video formats.

### Installing the Google Speech to Text Module
1. Install ffmpeg on you server.
    - For debian based systems:
        - `apt-get install -y ffmpeg`
    - For redhat based systems:
        - `dnf install -y ffmpeg`

    Test : 
    ```
    ffmpeg -i audiofile.mp3
    ```

2. Download the module into your modules/contrib folder. Use composer. This will install google cloud dependencies: https://github.com/googleapis/google-cloud-php-speech

3. For this module to work you need to have Google API service Credentials. If you don't have this, o <a href="https://console.cloud.google.com/apis/credentials" target="_blank">here</a>' and do that. More information <a href="https://cloud.google.com/speech-to-text/docs/quickstart" target="_blank">here</a> on how to do that.
